#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <stdbool.h>

void kever(char *str,size_t len);

int main() {

    srand(time(0));

    char szo[51] = {0};
    while (scanf("%s", szo) != EOF) {

        size_t len = strlen(szo);
        size_t last = len - 2;

        for(int i = 0;szo[i] != 0;i++) {
            if(szo[i] == '.' || szo[i] == '!' || szo[i] == ',' || szo[i] == '?'){
                //Utolso karakter + elso lejon
                last = i -2;
                break;
            }
        }
        kever(szo,last);
        printf("%s\n", szo);
    }

    return 0;
}

void kever(char *str, size_t size) {
    //Nincs mit osszkeverni
    if(size == 1)
        return;
    char eredeti[51] = {0};
    strcpy(eredeti,str);
    //Egyforma e a ketto
    while(strcmp(eredeti,str) == 0) {
        for(int i =0;i < size / 2;i++) {
            size_t elso = rand() % size + 1;
            size_t masodik = elso;
            while(masodik == elso) {
                masodik = rand() % size + 1;
            }
            char tmp = str[elso];
            str[elso] = str[masodik];
            str[masodik] = tmp;
        }
    }
}
